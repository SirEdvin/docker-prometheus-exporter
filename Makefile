build:
	docker build -t registry.gitlab.com/siredvin/docker-prometheus-exporter:v0.1.8 .
push:
	docker push registry.gitlab.com/siredvin/docker-prometheus-exporter:v0.1.8
test-run:
	docker run -it --rm --net host -v /var/run/docker.sock:/var/run/docker.sock --name test-docker-prometheus-exporter registry.gitlab.com/siredvin/docker-prometheus-exporter:v0.1.8
clean:
	docker rm -f test-pastereport
check:
	pylint app.py
	pycodestyle app.py
	mypy app.py --ignore-missing-imports
