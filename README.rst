==========================
Docker Prometheus Exporter
==========================

:Project version: v0.1.8

Base prometheus exporter to collect docker container metrics.

Getting Started
---------------

Just run exporter via docker container

.. code:: bash

    docker run --net host -d --name docker-exporter \
        -v /var/run/docker.sock:/var/run/docker.sock \
        registry.gitlab.com/siredvin/docker-prometheus-exporter:v0.1.8

And view metrics on :code:`127.0.0.1:9517/metrics`


Special cases
-------------

In some cases, we need a container filter. You can set environment variable :code:`EXPORTER_CONTAINER_IGNORE_PATTERN` to use regex that will filter containers.


Metrics list
------------

This service expose detailed report of memory usage, cpu usage and network package traffic. Also, expose data about container status.
Simple example you can find at METRICS.rst_

.. _METRICS.rst: ./METRICS.rst
