Metrics example
---------------

Just snapshot from my laptop

.. code::

    # HELP process_virtual_memory_bytes Virtual memory size in bytes.
    # TYPE process_virtual_memory_bytes gauge
    process_virtual_memory_bytes 253063168.0
    # HELP process_resident_memory_bytes Resident memory size in bytes.
    # TYPE process_resident_memory_bytes gauge
    process_resident_memory_bytes 33374208.0
    # HELP process_start_time_seconds Start time of the process since unix epoch in seconds.
    # TYPE process_start_time_seconds gauge
    process_start_time_seconds 1512721428.78
    # HELP process_cpu_seconds_total Total user and system CPU time spent in seconds.
    # TYPE process_cpu_seconds_total counter
    process_cpu_seconds_total 0.22
    # HELP process_open_fds Number of open file descriptors.
    # TYPE process_open_fds gauge
    process_open_fds 18.0
    # HELP process_max_fds Maximum number of open file descriptors.
    # TYPE process_max_fds gauge
    process_max_fds 1024.0
    # HELP python_info Python platform information
    # TYPE python_info gauge
    python_info{implementation="CPython",major="3",minor="6",patchlevel="3",version="3.6.3"} 1.0
    # HELP container_up Container up gauge
    # TYPE container_up gauge
    container_up{container_name="pastereport"} 0.0
    container_up{container_name="rethinkdb"} 0.0
    container_up{container_name="anji-consul"} 0.0
    container_up{container_name="node_exporter"} 1.0
    # HELP docker_collector_working Internal background task about docker stats collecting worked
    # TYPE docker_collector_working gauge
    docker_collector_working 1.0
    # HELP container_memory_usage_detailed_in_bytes Container memory detailed usage in bytes
    # TYPE container_memory_usage_detailed_in_bytes gauge
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="active_anon"} 6160384.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="active_file"} 2293760.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="cache"} 4620288.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="dirty"} 0.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="hierarchical_memory_limit"} 9.223372036854772e+18
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="hierarchical_memsw_limit"} 9.223372036854772e+18
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="inactive_anon"} 10285056.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="inactive_file"} 2326528.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="mapped_file"} 4567040.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="pgfault"} 18947.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="pgmajfault"} 82.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="pgpgin"} 20994.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="pgpgout"} 19428.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="rss"} 16445440.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="rss_huge"} 8388608.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="shmem"} 0.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="swap"} 98304.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_active_anon"} 6160384.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_active_file"} 2293760.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_cache"} 4620288.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_dirty"} 0.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_inactive_anon"} 10285056.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_inactive_file"} 2326528.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_mapped_file"} 4567040.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_pgfault"} 18947.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_pgmajfault"} 82.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_pgpgin"} 20994.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_pgpgout"} 19428.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_rss"} 16445440.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_rss_huge"} 8388608.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_shmem"} 0.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_swap"} 98304.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_unevictable"} 0.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="total_writeback"} 0.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="unevictable"} 0.0
    container_memory_usage_detailed_in_bytes{container_name="node_exporter",memory_type="writeback"} 0.0
    # HELP container_memory_usage_in_bytes Container memory usage in bytes
    # TYPE container_memory_usage_in_bytes gauge
    container_memory_usage_in_bytes{container_name="node_exporter"} 22331392.0
    # HELP container_memory_limit_in_bytes Container memory limit in bytes
    # TYPE container_memory_limit_in_bytes gauge
    container_memory_limit_in_bytes{container_name="node_exporter"} 16781852672.0
    # HELP container_network_package_stats Container network package stats
    # TYPE container_network_package_stats gauge
    # HELP container_total_cpu_usage Container total CPU usage
    # TYPE container_total_cpu_usage gauge
    container_total_cpu_usage{container_name="node_exporter"} 547000617510.0
    # HELP container_cpu_usage Container CPU usage
    # TYPE container_cpu_usage gauge
    container_cpu_usage{container_name="node_exporter",cpu="cpu0"} 137485476514.0
    container_cpu_usage{container_name="node_exporter",cpu="cpu1"} 135250904548.0
    container_cpu_usage{container_name="node_exporter",cpu="cpu2"} 137678699005.0
    container_cpu_usage{container_name="node_exporter",cpu="cpu3"} 136585537443.0
