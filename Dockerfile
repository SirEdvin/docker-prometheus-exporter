FROM alpine:3.7

# Install python

RUN apk add --no-cache python3 python3-dev gcc make musl-dev linux-headers && \
    pip3 install --upgrade pip

ADD requirements.txt /requirements.txt

RUN python3 -m pip install -r /requirements.txt

ADD "app.py" "/"

EXPOSE 9517

WORKDIR /

CMD ["python3", "app.py"]
