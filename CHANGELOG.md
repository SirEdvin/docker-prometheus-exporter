# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.8] - 2018-04-2

### Fixed

- Another mysteriously disappear data issue.

## [0.1.7] - 2018-04-02

### Fixed

- Fix some cases when container data mysteriously disappear.

## [0.1.6] - 2018-04-02

### Added

- Ability to ignore some containers via `EXPORTER_CONTAINER_IGNORE_PATTERN` env variable

### Fixed

- Problem with not found container

## [0.1.5] - 2018-04-02

### Added

- Metric `docker_collector_working` about internal collector status

### Fixed

- Problem with `restarted` but not `runned` containers that cause docker exporter freeze

## [0.1.4] - 2018-02-19

### Added

- Logging output

## [0.1.3] - 2018-02-02

### Changed

- Fix docker container setup

## [0.1.2] - 2018-02-02

### Added

- Clenaup stopped and removed containers metrics (#1)

### Fixed

- Problem with containers without limit (#2)
- Dependency version

## [0.1.1] - 2017-12-10

### Fixed

- Close all resources correctly
- Run collection metrics task correctly

[Unreleased]: https://gitlab.com/SirEdvin/docker-prometheus-exporter/compare/v0.1.8...HEAD
[0.1.8]: https://gitlab.com/SirEdvin/docker-prometheus-exporter/compare/v0.1.7...v0.1.8
[0.1.7]: https://gitlab.com/SirEdvin/docker-prometheus-exporter/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/SirEdvin/docker-prometheus-exporter/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/SirEdvin/docker-prometheus-exporter/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/SirEdvin/docker-prometheus-exporter/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/SirEdvin/docker-prometheus-exporter/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/SirEdvin/docker-prometheus-exporter/compare/v0.1.1...v0.1.2
