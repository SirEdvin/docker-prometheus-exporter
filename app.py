import asyncio
import logging
import os
from concurrent.futures import CancelledError
import re
from typing import Set

import aiodocker
from aiodocker.containers import DockerContainer
from aiodocker.exceptions import DockerError
from prometheus_client import Gauge
from prometheus_async.aio import track_inprogress
from sanic import Sanic, response
from sanic.request import Request
from sanic_prometheus import MonitorSetup

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.8"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"

app = Sanic()

CONTAINER_UP = Gauge('container_up', 'Container up gauge', ['container_name'])
DOCKER_COLLECTOR_WORKING = Gauge("docker_collector_working", "Internal background task about docker stats collecting worked", [])
CONTAINER_MEMORY_USAGE_DETAILED = Gauge('container_memory_usage_detailed_in_bytes', 'Container memory detailed usage in bytes', ['container_name', 'memory_type'])
CONTAINER_MEMORY_USAGE = Gauge('container_memory_usage_in_bytes', 'Container memory usage in bytes', ['container_name'])
CONTAINER_MEMORY_LIMIT = Gauge('container_memory_limit_in_bytes', 'Container memory limit in bytes', ['container_name'])
CONTAINER_NETWORK_PACKAGE = Gauge('container_network_package_stats', 'Container network package stats', ['container_name', 'interface', 'stat_type'])
CONTAINER_TOTAL_CPU_USAGE = Gauge('container_total_cpu_usage', 'Container total CPU usage', ['container_name'])
CONTAINER_CPU_USAGE = Gauge('container_cpu_usage', 'Container CPU usage', ['container_name', 'cpu'])

LOG_FORMAT = '[%(asctime)s] %(name)s:%(levelname)s: %(message)s'

METRICS_WITHOUT_UP = {
    CONTAINER_MEMORY_USAGE_DETAILED, CONTAINER_MEMORY_USAGE, CONTAINER_MEMORY_LIMIT,
    CONTAINER_NETWORK_PACKAGE, CONTAINER_TOTAL_CPU_USAGE, CONTAINER_CPU_USAGE
}


CONTAINER_IGNORE_PATTERN = re.compile(os.environ.get("EXPORTER_CONTAINER_IGNORE_PATTERN", "^&"))


DEFAULT_COLLECT_SLEEP = 5

_log = logging.getLogger('docker exporter')


async def collect_stats_from_container(container: DockerContainer) -> None:
    container_stats = await container.stats(stream=False)
    if not container_stats:
        _log.warning("Strange container without stats")
        _log.warning(container)
        return
    container_name = container_stats['name'][1:]
    _log.info('Process data about %s', container_name)
    CONTAINER_MEMORY_LIMIT.labels(container_name).set(container_stats['memory_stats'].get('limit', -1))  # pylint: disable=no-member
    try:
        CONTAINER_MEMORY_USAGE.labels(container_name).set(container_stats['memory_stats']['usage'])  # pylint: disable=no-member
    except KeyError:
        _log.warning("Unexpected problem with corrupted container data")
        _log.warning(container_name)
        return
    for stat, value in container_stats['memory_stats']['stats'].items():
        CONTAINER_MEMORY_USAGE_DETAILED.labels(container_name, stat).set(value)  # pylint: disable=no-member
    for interface, interface_stat in container_stats.get('networks', {}).items():
        for stat_type, value in interface_stat.items():
            CONTAINER_NETWORK_PACKAGE.labels(container_name, interface, stat_type).set(value)  # pylint: disable=no-member
    cpu_usage = container_stats['cpu_stats']['cpu_usage']
    CONTAINER_TOTAL_CPU_USAGE.labels(container_name).set(cpu_usage['total_usage'])  # pylint: disable=no-member
    for index, value in enumerate(cpu_usage['percpu_usage']):
        CONTAINER_CPU_USAGE.labels(container_name, f"cpu{index}").set(value)  # pylint: disable=no-member


def drop_container_metric(container_name: str, metric: Gauge) -> None:
    metric._metrics = {k: v for k, v in metric._metrics.items() if k[0] != container_name}  # pylint: disable=protected-access


def drop_container_metrics(container_name: str, with_up_metric: bool = False) -> None:
    for gauge_metric in METRICS_WITHOUT_UP:
        drop_container_metric(container_name, gauge_metric)
    if with_up_metric:
        drop_container_metric(container_name, CONTAINER_UP)


@track_inprogress(DOCKER_COLLECTOR_WORKING)
async def collect_docker_metrics(docker_client: aiodocker.Docker) -> None:
    try:
        old_names: Set[str] = set()
        while True:
            _log.info('Execute container fetch')
            containers = await docker_client.containers.list(all=True)
            new_names: Set[str] = set()
            for container in containers:
                try:
                    container_data = await container.show()
                except DockerError as docker_exc:
                    # In some magic cases, container may be in list but don't exists in reallity
                    # so do error processing
                    if docker_exc.status == 404:
                        continue
                    raise
                container_name = container_data['Name'][1:]
                if CONTAINER_IGNORE_PATTERN.match(container_name):
                    _log.info("Skip container with name %s because of container ignore pattern", container_name)
                    continue
                new_names.add(container_name)
                if container_data['State']['Running'] and not container_data['State']['Restarting']:
                    CONTAINER_UP.labels(container_name).set(1)  # pylint: disable=no-member
                    await collect_stats_from_container(container)
                else:
                    CONTAINER_UP.labels(container_name).set(0)  # pylint: disable=no-member
                    drop_container_metrics(container_name)
            for removed_container_name in old_names - new_names:
                _log.info("Remove data about removed container %s", removed_container_name)
                drop_container_metrics(removed_container_name, with_up_metric=True)
            old_names = new_names
            await asyncio.sleep(DEFAULT_COLLECT_SLEEP)
    except CancelledError as exp:
        await docker_client.close()
        raise
    except Exception as exp:
        _log.exception(exp)  # type: ignore
        raise


@app.listener('after_server_start')
async def start_timers(sanic_app, loop) -> None:
    sanic_app.metric_collection_task = loop.create_task(collect_docker_metrics(aiodocker.Docker()))


@app.listener('after_server_stop')
async def stop_timers(sanic_app, _loop) -> None:
    sanic_app.metric_collection_task.cancel()


@app.route('/about')
async def about(_request: Request) -> response.HTTPResponse:
    return response.json({
        'name': 'Docker Prometheus Exporter',
        'version': __version__,
        'license': __license__
    })


if __name__ == "__main__":
    MonitorSetup(app).expose_endpoint()
    handler = logging.StreamHandler()  # pylint: disable=invalid-name
    handler.setFormatter(logging.Formatter(LOG_FORMAT))
    _log.addHandler(handler)
    _log.setLevel(logging.INFO)
    app.run(host="0.0.0.0", port=9517, debug=True)
